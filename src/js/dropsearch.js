function DropSearch(element,options){
    if(this instanceof DropSearch == false){
        return new DropSearch(element,options);
    }

    var that = this;
    var Element = null;
    var Loader = null;
    var SearchField = null;
    var ResultContainer = null;
    var Items = [];
    var PreItems = null;
    var Template = '<div>{{data}}</div>';
    var TemplateUrl = null;
    var Placeholder = '';
    var OnSelect = null;
    var Autoselect = false;
    var SelectedIndex = null;
    var SearchFields = null;
    var Stopwords = null;
    var PreProcess = null;
    var Autofocus = false;
    var TypeDelay = 300;

    var Minlength = 1;

    var delayTimer;

    function setArgs(x){
        for(key in x){
            if(key in that){
                that[key] = x[key];
            }
        };
    };

    function init(){
        setArgs(options);
        renderSelf();
        bindEvents();
    }

    function bindEvents(){
        SearchField.addEventListener('keyup',function(e){
            if(new RegExp('38|40|13').test(e.keyCode) == false){
                    if(SearchField.value.length >= Minlength){
                        clearTimeout(delayTimer);
                        delayTimer = setTimeout(function(){
                            loading(true);
                            if(typeof PreItems == 'string'){
                                var request = new XMLHttpRequest();
                                    request.onreadystatechange = function(){
                                        if(request.readyState == 4){
                                            if(request.status >= 200 && request.status < 400){
                                                var response = JSON.parse(request.responseText);

                                                if(typeof PreProcess === 'function'){
                                                    response = PreProcess(response);
                                                }

                                                renderResults(response);
                                            }else{
                                                loading(false);
                                            }
                                        }
                                    }
                                    request.open('GET',PreItems + SearchField.value);
                                    request.send();
                            }else if(PreItems instanceof Array){
                                Items = PreItems;
                                renderResults(search(SearchField.value));
                            }else{
                                Items = PreItems(SearchField.value);
                            }
                        },TypeDelay);
                    }else{
                        ResultContainer.innerHTML = '';
                    }
                
            }
        });

        SearchField.addEventListener('keydown',function(e){
            switch(e.keyCode){
                case 38:
                    e.preventDefault();
                    selectPrev();
                    break;
                case 40:
                    e.preventDefault();
                    selectNext();
                    break;
                case 9:
                    selectItem();
                    close();
                    break;
            }   
        });

        SearchField.addEventListener('keypress',function(e){
            if(e.keyCode == 13){
                selectItem();
            }
        });

        ResultContainer.addEventListener('mouseleave',function(){
            SelectedIndex = null;
            updateSelection();
        });
    }

    function cleanQuery(query){
        if(!Stopwords){return query;}

        return query.split(' ').filter(function(word){
            return Stopwords.indexOf(word) < 0;
        }).map(function(word){
            return '(?=.*' + word + ')';
        }).join('') + '.*';
    }

    function search(query){
        query = cleanQuery(query);
        if(!query){return [];}
        var regex = new RegExp(query,'i');
        

        return Items.filter(function(x){
            if(SearchFields){
                var state = false;
                SearchFields.forEach(function(field){
                    var keys = field.split('.');
                    var value = keys.reduce(function(val,item){
                        return val[item];
                    },x);

                    if(regex.test(value)){
                        state = true;
                    }
                });

                return state;
            }else{
                return regex.test(x);
            }
        });
    }

    function loading(state){
        if(state && !Loader){
            Loader = document.createElement('div');
            Loader.classList.add('ds-loader');
            Element.insertBefore(Loader,Element.firstChild);
        }else if(!state && Loader){
            Element.removeChild(Loader);
            Loader = null;
        }
    }

    function close(){
        SearchField.value = '';
        ResultContainer.innerHTML = '';
        SelectedIndex = null;
    }

    function renderSelf(){
        SearchField = document.createElement('input');
        SearchField.setAttribute('type','text');
        SearchField.setAttribute('placeholder',Placeholder);
        SearchField.classList.add('ds-search');

        ResultContainer = document.createElement('div');
        ResultContainer.classList.add('ds-results');

        Element.classList.add('dropsearch');
        Element.appendChild(SearchField);
        Element.appendChild(ResultContainer);

        if(Autofocus){
            SearchField.focus();
        }
    }

    function renderResults(results){
        Items = results;
        ResultContainer.innerHTML = '';
        SelectedIndex = null;

        Items.forEach(function(item,i){
             var el = document.createElement('div');
                el.classList.add('ds-result');
                el.setAttribute('result-index',i);
                el.innerHTML = parseTemplate(item,Template);

            el.addEventListener('mouseenter',function(){
                SelectedIndex = parseInt(this.getAttribute('result-index'));
                updateSelection();
            });

            el.addEventListener('click',function(){
                SelectedIndex = parseInt(this.getAttribute('result-index'));
                selectItem();
            });

            ResultContainer.appendChild(el);
        });

        if(Autoselect){
            selectNext();
        }

        loading(false);
    }

    function parseTemplate(data,template){
        var re = /{{data\.?(.+?)?}}/;
        var m;
        while(m = re.exec(template)){
            var selector = '';
            var value = data;
            if(m[1]){
                var keys = m[1].split('.');
                selector = '.' + m[1];
                value = keys.reduce(function(val,item){
                    return val[item];
                },data);
            }

            var replace = new RegExp('{{data' + selector + '}}');
            template = template.replace(replace,value);
        } 

        return template;
    }

    function selectNext(){
        if(SelectedIndex == null){SelectedIndex = -1;}
        var max = ResultContainer.children.length - 1;

        SelectedIndex = (SelectedIndex < max)?SelectedIndex + 1:max;
        updateSelection();
    }

    function selectPrev(){
        if(SelectedIndex == null){SelectedIndex = -1;}
        var max = ResultContainer.children.length - 1;

        SelectedIndex = (SelectedIndex == 0)?null:SelectedIndex - 1;
        updateSelection();
    }

    function updateSelection(){
        var oldSelection = ResultContainer.querySelector('.ds-selected');
        if(oldSelection){
            oldSelection.classList.remove('ds-selected');
        }
        var el = ResultContainer.children[SelectedIndex];
        if(el){
            el.classList.add('ds-selected');
        }
    }  

    function selectItem(){
        if(OnSelect){
            if(SelectedIndex != null){
                OnSelect(Items[SelectedIndex]);
            }else{
                OnSelect(SearchField.value);
            }
        }
        close();
    }

    Object.defineProperties(that,{
        "element": {
            get: function(){
                return Element;
            },
            set: function(x){
                if(typeof x == 'string'){
                    var y = document.querySelector(x);
                    if(y){
                        Element = y;
                    }else{
                        console.error("Element not found");
                    }
                }else if(x instanceof HTMLElement){
                    Element = x;
                }else{
                    console.error("Invalid type");
                }
            }
        },
        "items": {
            get: function(){
                return PreItems;
            },
            set: function(x){
                if(typeof x == 'string' || x instanceof Array || typeof x === 'function'){
                    PreItems = x;
                }else{
                    console.error('Invalid items format');
                }
            }
        },
        "minlength": {
            get: function(){
                return Minlength
            },
            set: function(x){
                if(typeof x == 'string' || typeof x == 'number'){
                    Minlength = parseInt(x);
                }
            }
        },
        "template": {
            get: function(){
                return Template;
            },
            set: function(x){
                Template = x;
            }
        },
        "templateUrl": {
            get: function(){
                return TemplateUrl;
            },
            set: function(x){
                 var request = new XMLHttpRequest();
                    request.onreadystatechange = function(){
                        if(request.readyState == 4 && request.status >= 200 && request.status < 400){
                            Template  = request.responseText;
                        }
                    }

                    request.open('GET',x);
                    request.send();
            }
        },
        "placeholder": {
            get: function(){
                return Placeholder;
            },
            set: function(x){
                if(typeof x == 'string'){
                    Placeholder = x;
                    if(SearchField){
                        SearchField.setAttribute('placeholder',Placeholder);
                    }
                }
            }
        },
        "onselect": {
            get: function(){
                return OnSelect;
            },
            set: function(x){
                if(x instanceof Function){
                    OnSelect = x;
                }
            }
        },
        "preprocess": {
            get: function(){
                return PreProcess;
            },
            set: function(x){
                if(x instanceof Function){
                    PreProcess = x;
                }
            }
        },
        "autoselect": {
            get: function(){
                return Autoselect;
            },
            set: function(x){
                Autoselect = (x)?true:false;
            }
        },
        "searchFields": {
            get: function(){
                return SearchFields;
            },
            set: function(x){
                if(typeof x == 'string'){
                    SearchFields = [x];
                }else if(x instanceof Array){
                    SearchFields = x;
                }else{
                    SearchFields = null;
                }
            }
        },
        "autofocus": {
            get: function(){
                return Autofocus;
            },
            set: function(x){
                Autofocus = (x)?true:false;
            }
        },
        "stopwords": {
            get: function(){
                return Stopwords;
            },
            set: function(x){
                if(x instanceof Array){
                    Stopwords = x;
                }else{
                    Stopwords = null;
                }
            }
        },
        "typedelay": {
            get: function(){
                return TypeDelay;
            },
            set: function(x){
                if(typeof x == 'string' || typeof x == 'number'){
                    TypeDelay = parseInt(x);
                }
            }
        }
    });



    that.element = element;
    init();
    return Object.freeze(that);
}